variable "allocated_storage" {
  type    = string
  default = ""
}

variable "privat_ids" {
  type    = list(any)
  default = []
}

variable "storage_type" {
  type    = string
  default = ""
}

variable "engine" {
  type    = string
  default = ""
}

variable "engine_version" {
  type    = string
  default = ""
}

variable "instance_class" {
  type    = string
  default = ""
}


variable "vpc_security_group_ids" {
  default = ""
}
