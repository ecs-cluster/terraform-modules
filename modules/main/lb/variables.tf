variable "security_groups" {
  type    = string
  default = "aws_security_group.MainSG.id"
}

variable "subnets" {
  type    = list(any)
  default = []
}

variable "domain_name" {
  type    = string
  default = "example.com"
}

variable "vpc_id" {
  type    = string
  default = "aws_vpc.main.id"
}

variable "domain_name_app2" {
  type    = string
  default = "example.com/app2"
}

variable "domain_name_app3" {
  type    = string
  default = "example.com/app3"
}
