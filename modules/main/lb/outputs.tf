output "name" {
  value = aws_lb.Main_lb.id
}

output "DNS_Name" {
  value = aws_lb.Main_lb.dns_name
}

output "target_group_arn" {
  value = aws_lb_target_group.main_app.arn
}

output "target_groupapp2_arn" {
  value = aws_lb_target_group.app2.arn
}

output "target_groupapp3_arn" {
  value = aws_lb_target_group.app3.arn
}
