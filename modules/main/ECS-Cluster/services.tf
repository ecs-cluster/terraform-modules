/* resource "aws_ecs_service" "ecs_service" {
  name            = "my-ecs-service"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.main_task_definition
  desired_count   = 2

  network_configuration {
    subnets         = var.subnets
    security_groups = var.security_group
  }

  force_new_deployment = true
  placement_constraints {
    type = "distinctInstance"
  }

  triggers = {
    redeployment = timestamp()
  }

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
    weight            = 100
  }

  load_balancer {
    target_group_arn = var.target_group_arn
    container_name   = "main"
    container_port   = 80
  }
}


resource "aws_ecs_service" "ecs_service" {
  name            = "my-ecs-serviceapp2"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.app2_task_definition
  desired_count   = 2

  network_configuration {
    subnets         = var.subnets
    security_groups = var.security_group
  }

  force_new_deployment = true
  placement_constraints {
    type = "distinctInstance"
  }

  triggers = {
    redeployment = timestamp()
  }

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
    weight            = 100
  }

  load_balancer {
    target_group_arn = var.target_groupapp2_arn
    container_name   = "app2"
    container_port   = 80
  }
}

resource "aws_ecs_service" "ecs_service" {
  name            = "app3"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.app3_task_definition
  desired_count   = 2

  network_configuration {
    subnets         = var.subnets
    security_groups = var.security_group
  }

  force_new_deployment = true
  placement_constraints {
    type = "distinctInstance"
  }

  triggers = {
    redeployment = timestamp()
  }

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
    weight            = 100
  }

  load_balancer {
    target_group_arn = var.target_groupapp3_arn
    container_name   = "app3"
    container_port   = 80
  }
}
 */
