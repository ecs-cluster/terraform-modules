variable "auto_scaling_group_arn" {
  type    = string
  default = "aws_autoscaling_group.ecs_asg.arn"
}

variable "subnets" {
  type    = list()
  default = []
}

variable "security_group" {
  default = ""
}

variable "target_group_arn" {
  type    = string
  default = ""
}

variable "target_groupapp2_arn" {
  type    = string
  default = ""
}

variable "target_groupapp3_arn" {
  type    = string
  default = ""
}
