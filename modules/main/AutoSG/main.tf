data "aws_ami" "linux" {
  owners      = [var.owners]
  most_recent = "true"

  filter {
    name   = "name"
    values = [var.values]
  }
}


resource "aws_launch_template" "main_server" {
  name                   = "aws_linix"
  instance_type          = var.instance
  image_id               = data.aws_ami.linux.id
  vpc_security_group_ids = [var.SGroup_id]
  key_name               = "LinuxServer"

  block_device_mappings {
    device_name = "/dev/sdf"

    ebs {
      volume_size = 20
    }
  }


  tags = {
    Name = "ClusterServer"
  }
}

resource "aws_autoscaling_group" "ClusterASG" {
  name                      = "ASG-ClusterASG"
  desired_capacity          = var.capacity
  max_size                  = var.max
  min_size                  = var.min
  health_check_type         = "ELB"
  health_check_grace_period = 300

  vpc_zone_identifier = var.vpc_zone_identifier

  launch_template {
    id      = aws_launch_template.main_server.id
    version = aws_launch_template.main_server.latest_version
  }

  dynamic "tag" {
    for_each = var.tags

    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }

  }
  lifecycle {
    create_before_destroy = true
  }
}

#----------------------------------------------------
resource "aws_autoscaling_policy" "ClusterASG_policy_up" {
  name                   = "web_policy_up"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.ClusterASG.name
}
resource "aws_cloudwatch_metric_alarm" "ClusterASG_cpu_alarm_up" {
  alarm_name          = "web_cpu_alarm_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "60"
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.ClusterASG.name
  }
  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions     = [aws_autoscaling_policy.ClusterASG_policy_up.arn]
}



#----------------------------------------------------
resource "aws_autoscaling_policy" "ClusterASG_policy_down" {
  name                   = "web_policy_down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.ClusterASG.name
}
resource "aws_cloudwatch_metric_alarm" "ClusterASG_cpu_alarm_down" {
  alarm_name          = "web_cpu_alarm_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "10"
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.ClusterASG.name
  }
  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions     = [aws_autoscaling_policy.ClusterASG_policy_down.arn]
}
